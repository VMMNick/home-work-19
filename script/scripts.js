function estimateProjectCompletionTime(teamSpeed, backlog, deadline) {
    // Розраховуємо загальну швидкість команди
    const teamVelocity = teamSpeed.reduce((acc, speed) => acc + speed, 0);
  
    // Розраховуємо загальну кількість балів у беклозі
    const totalPointsInBacklog = backlog.reduce((acc, points) => acc + points, 0);
  
    // Розраховуємо кількість робочих днів до дедлайну
    const workingDays = workingDaysUntilDeadline(new Date(), deadline);
  
    // Розраховуємо кількість робочих годин до дедлайну
    const workingHours = workingDays * 8;
  
    // Розраховуємо кількість балів, яку команда зможе виконати до дедлайну
    const achievablePoints = Math.min(teamVelocity * workingDays, totalPointsInBacklog);
  
    if (achievablePoints >= totalPointsInBacklog) {
      // Команда встигне виконати всі завдання до дедлайну
      const daysToDeadline = workingDaysUntilDeadline(new Date(), deadline);
      console.log(`Усі завдання будуть успішно виконані за ${daysToDeadline} днів до настання дедлайну!`);
    } else {
      // Команда не встигне виконати всі завдання до дедлайну
      const additionalHours = (totalPointsInBacklog - achievablePoints) / teamVelocity;
      console.log(`Команді розробників доведеться витратити додатково ${additionalHours} годин після дедлайну, щоб виконати всі завдання в беклозі`);
    }
  }
  
  // Функція для розрахунку кількості робочих днів між двома датами
  function workingDaysUntilDeadline(startDate, endDate) {
    let workingDays = 0;
    let currentDate = new Date(startDate);
    while (currentDate <= endDate) {
      // Перевіряємо, чи поточний день не є вихідним (субота або неділя)
      if (currentDate.getDay() !== 0 && currentDate.getDay() !== 6) {
        workingDays++;
      }
      // Переходимо до наступного дня
      currentDate.setDate(currentDate.getDate() + 1);
    }
    return workingDays;
  }
  
  // Приклад використання функції
  const teamSpeed = [4, 5, 6]; // Швидкість роботи розробників
  const backlog = [10, 8, 12]; // Беклог
  const deadline = new Date('2023-09-30'); // Дедлайн
  
  estimateProjectCompletionTime(teamSpeed, backlog, deadline);
  function calculateDeadline(роботящие, беклог, дедлайн) {
    // Визначимо загальну кількість сторі поінтів у беклозі
    const загальнаКількістьСтр = беклог.reduce((сума, сторіПоінти) => сума + сторіПоінти, 0);
  
    // Визначимо загальну кількість робочих годин до дедлайну
    const сьогодні = new Date();
    let робочіГодини = 0;
    while (сьогодні < дедлайн) {
      // Перевірка, чи сьогодні будній день (пн-пт)
      if (сьогодні.getDay() >= 1 && сьогодні.getDay() <= 5) {
        робочіГодини += роботящие.reduce((сума, швидкість) => сума + швидкість, 0);
      }
      // Додаємо один день
      сьогодні.setDate(сьогодні.getDate() + 1);
    }
  
    // Визначимо кількість днів і годин, потрібних для завершення всіх завдань
    const дні = Math.floor(загальнаКількістьСтр / робочіГодини);
    const години = (загальнаКількістьСтр % робочіГодини) / роботящие.length;
  
    if (дні <= 0) {
      const годиніПісляДедлайну = (дедлайн - сьогодні) / (60 * 60 * 1000);
      return `Команді розробників доведеться витратити додатково ${годиніПісляДедлайну} годин після дедлайну, щоб виконати всі завдання в беклозі`;
    } else {
      return `Усі завдання будуть успішно виконані за ${дні} днів до настання дедлайну!`;
    }
  }
  
  // Приклад використання функції
  const роботящие = [4, 6, 8]; // Швидкість роботи розробників (у сторі поінтів за день)
  const беклог = [30, 20, 40]; // Беклог (кількість сторі поінтів у завданнях)
  const дедлайн = new Date('2023-09-30'); // Дедлайн
  
  const результат = calculateDeadline(роботящие, беклог, дедлайн);
  console.log(результат);
  